" All system-wide defaults are set in $VIMRUNTIME/debian.vim (usually just
" /usr/share/vim/vimcurrent/debian.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vim/vimrc), since debian.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing debian.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

set nocompatible
set ls=2
set wrap
set tabstop=4
set expandtab
set shiftwidth=3
set cindent
set ignorecase
set ruler
set shell=/bin/zsh
set showmatch
set showmode
set number
set backspace=indent,eol,start
set nostartofline
set hlsearch
set incsearch
set autoread 
set more
set backup
set backupext=~
set scrolloff=5
set sidescrolloff=5
set history=200
set smarttab
set updatecount=100
set encoding=utf8
set noexrc



command! Wq wq
syntax on
set background=dark
set pastetoggle=<F10>



"" Codefaltungsfunktion für pl, pm, c, h, cpp und java Files
au BufNewFile,BufRead   *           syn sync fromstart
au BufNewFile,BufRead   *           set foldmethod=manual
au BufNewFile,BufRead   *.c,*.h,*.cpp,*.cc,*.java   set foldmethod=syntax
au BufNewFile,BufRead   *.pl,*.pm      set foldmethod=indent
au BufNewFile,BufRead   *.c,*.h,*.cpp,*.cc,*.pl,*.pm,*.java  syn region myFold start="{" end="}" transparent fold
au BufNewFile,BufRead   *           set foldlevel=99
au BufNewFile,BufRead   *.html,*.htm   set sw=1
au BufNewFile,BufRead   *.po           set tw=80
au BufNewFile,BufRead   *.hg           set ft=cpp
au BufNewFile,BufRead   *.ccg          set ft=cpp
fun! ToggleFold()
   if foldlevel('.') == 0
      normal! l
   else
     if foldclosed('.') < 0
       . foldclose
     else
       . foldopen
     endif
   endif
   " Clear status line
   echo
endfun
"" <Space> Klappt den Codebereich ein oder aus
noremap <space> :call ToggleFold()<CR>1




" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
"    \| exe "normal g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules according to the
" detected filetype. Per default Debian Vim only load filetype specific
" plugins.
"if has("autocmd")
"  filetype indent on
"endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
"set showcmd		" Show (partial) command in status line.
"set showmatch		" Show matching brackets.
"set ignorecase		" Do case insensitive matching
"set smartcase		" Do smart case matching
"set incsearch		" Incremental search
"set autowrite		" Automatically save before commands like :next and :make
"set hidden             " Hide buffers when they are abandoned
set mouse=a		" Enable mouse usage (all modes) in terminals

" Source a global configuration file if available
" XXX Deprecated, please move your changes here in /etc/vim/vimrc

filetype off                  " required!

"set rtp+=~/.vim/bundle/vundle/
"call vundle#rc()

"set runtimepath+=~/.vim/bundle/ultisnips
" let Vundle manage Vundle
" required! 
"Bundle 'gmarik/vundle'

" My bundles here:
"
" original repos on GitHub
"Bundle 'tpope/vim-fugitive'
"Bundle 'Lokaltog/vim-easymotion'
"Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
"Bundle 'tpope/vim-rails.git'
"Show marks
"Bundle 'vim-scripts/ShowMarks'
"Easy Motion
"Bundle 'Lokaltog/vim-easymotion'
"Autoclose
"Bundle 'Townk/vim-autoclose'
"UltiSnips
"Bundle 'SirVer/ultisnips'
"Snipmate
"Bundle "MarcWeber/vim-addon-mw-utils"
"Bundle "tomtom/tlib_vim"
"Bundle "garbas/vim-snipmate"
"Bundle "honza/vim-snippets"
"Nerdtree
"Bundle 'scrooloose/nerdtree'
" vim-scripts repos
"Bundle 'L9'
"Bundle 'FuzzyFinder'
" non-GitHub repos
"Bundle 'git://git.wincent.com/command-t.git'
" Git repos on your local machine (i.e. when working on your own plugin)
" ...

filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install (update) bundles
" :BundleSearch(!) foo - search (or refresh cache first) for foo
" :BundleClean(!)      - confirm (or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle commands are not allowed.

"Indent
set autoindent
set si "Smart indent"

"UltiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

"Nerdtree
"autocmd VimEnter * NERDTree
"autocmd VimEnter * NERDTree
"autocmd VimEnter * wincmd p

let NERDTreeShowBookmarks = 1
let NERDTreeWinPos = 1
let NERDTreeAutoCenter = 1
let NERDTreeAutoCenterThreshold = 5
let NERDTreeHighlightCursorline = 1
let NERDTreeIgnore = ['\.pdf$', '\~$','\.tar','\.zip','\.jpg','\.png','\.svg']
let NERDTreeShowLineNumbers = 1
let NERDTreeSortOrder = ['\/$','\.tex$', '\.c$', '\.h$', '*']


set foldmethod=manual
inoremap <F9> <C-O>za
nnoremap <F9> za
onoremap <F9> <C-C>za
vnoremap <F9> zf

set wildmenu
set wildmode=longest,list:longest,list:full

"autocmd BufNewFile,BufRead *.tex set spell
"autocmd BufNewFile,BufRead *.tex set spelllang=de_de

"if filereadable("/etc/vim/vimrc.local")
"  source /etc/vim/vimrc.local
"endif


let g:powerline_pycmd="py3"
set laststatus=2
set nomodeline
